import React from "react";

import { AppContainer, AppTitle, AppSubtitle, AppDescription, AppDescriptionLink, AppImageContainer, AppImage } from "assets/styles/App.styles.jsx";

import space from "assets/images/space.png";
import fighter from "assets/images/fighter.png";
import computer from "assets/images/computer.png";
import woman from "assets/images/woman.png";

const App = () => (
  <AppContainer>
    <AppTitle>NEOPHASER</AppTitle>
    <AppSubtitle>or, audio effects as applied to the retina</AppSubtitle>
    <AppDescription>neophaser is a project designed to apply audio effect chains to image and video data. download <AppDescriptionLink href="https://gitlab.com/8o-collective/neophaser/-/releases">here</AppDescriptionLink> or do `pip install neophaser` and `python -m neophaser` if you know what that means. if you don't, don't worry. it will all be ok in the end. time for pretty photos!</AppDescription>
    <AppImageContainer>
      <AppImage src={space} />
      <AppImage src={fighter} />
      <AppImage src={computer} />
      <AppImage src={woman} />
    </AppImageContainer>
  </AppContainer>
);

export default App;

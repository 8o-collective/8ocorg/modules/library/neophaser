import styled from "styled-components";

const AppContainer = styled.div`
  background-color: black;
  margin: 0px;
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;
  overflow: scroll;
`;

const AppTitle = styled.div`
  display: block;
  position: absolute;
  top: 5vh;
  left: 50%;
  transform: translate(-50%, -50%);

  color: white;
  font-family: "IBM3270", monospace;
  font-size: max(5vw, 40px);
  text-align: center;
`;

const AppSubtitle = styled.div`
  display: block;
  position: absolute;
  top: 13vh;
  left: 50%;
  width: 100%;
  transform: translate(-50%, -50%);

  color: red;
  font-family: "IBM3270", monospace;
  font-size: max(2vw, 19px);
  text-align: center;
`;

const AppDescription = styled.div`
  display: block;
  position: absolute;
  top: 30vh;
  left: 50%;
  width: 100%;
  transform: translate(-50%, -50%);
  width: 60vw;

  color: white;
  font-family: "IBM3270", monospace;
  font-size: max(1.5vw, 13px);
  text-align: center;
`;

const AppDescriptionLink = styled.a`
  color: red;
`;

const AppImageContainer = styled.div`
  display: flex;
  position: absolute;
  top: 40vh;
  left: 50%;
  transform: translateX(-50%);

  justify-content: space-between;
  flex-direction: column;
  flex-grow: 1;
  flex-basis: 0;
`;

const AppImage = styled.img`
  display: block;
  width: 80vw;
`;

export { AppContainer, AppTitle, AppSubtitle, AppDescription, AppDescriptionLink, AppImageContainer, AppImage };

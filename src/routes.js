import React from "react";
import { Routes, Route } from "react-router-dom";

import App from "components/App.jsx";

const NotFound = () => {
  window.location.replace("http://invite.8oc.org");
};

const routes = (
  <Routes>
    <Route path="*" element={<NotFound />} />
    <Route exact path="/" element={<App />} />
  </Routes>
);

export { routes };
